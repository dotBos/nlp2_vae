# MIT License
#
# Copyright (c) 2017 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
#
# Edit Author: Bryan Cardenas Guevara, UvA
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time
from datetime import datetime
import argparse

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')

import torch
from torch import softmax
from torch import multinomial
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss
from torch.optim.lr_scheduler import LambdaLR
from torch.optim.lr_scheduler import ExponentialLR

from dataset import TextDataset
from model import TextGenerationModel

################################################################################
def accuracy(preds, targets, seq_length):
  accuracy = 0.0
  with torch.no_grad():
    comparison = torch.max(preds,1)[1] == targets.long()
    accuracy = torch.sum(comparison).item() / (comparison.shape[0]*seq_length)
  return accuracy

'''
def one_hot(batch,vocab_size):
  embedding = torch.nn.Embedding(vocab_size, 300)
  embedding.weight.data = torch.eye(vocab_size)
  return embedding(batch).transpose(1,0)
'''

def one_hot(batch, vocab_size):
  '''Input: batch_n x input_seq '''
  batch_n, seq_num = batch.shape
  one_hot = torch.zeros(seq_num, batch_n, vocab_size)
  for i in range(batch_n):
    for j in range(seq_num):
      one_hot[j,i,batch[i,j].item()] = 1
  return one_hot


def sample(logits, T):
  rand_sample = multinomial(softmax(logits / T, dim=1),1)
  return rand_sample

def convert_sentence(sentence,data):
      sliced_sentence = list(sentence)
      char_to_ix = data._char_to_ix
      vocab_size = data._vocab_size
      ix = torch.tensor([torch.tensor(char_to_ix[char]) for char in sliced_sentence])
      ix = one_hot(ix.unsqueeze(0), vocab_size)
      return ix

# FIX
def generate_text(model, config, dataset, use_temp=True, device="cuda",sentences = []):
  model.eval()
  feed_start_sentence = (True if len(sentences) > 0 else False)
  seq_length = config.seq_length
  vocab_size = dataset._vocab_size
  char_mapping = dataset._ix_to_char
  temperature = 0.5
  char = torch.randint(0,vocab_size-1,(1,))
  #char = torch.zeros((1,vocab_size))
  #char[0, random_character_number] = 1
  # Maybe we don't need to tell it how long the sequence shoould be
  resulting_sentences = []
  # uncomment if sentences is not empty
  #for start_sentence in sentences:
  result = []
  # prepare the correct shape for the lstm
  #print("new char: ", char.shape)

  new_char = char.unsqueeze(0).to(device)
  h = torch.zeros(config.lstm_num_layers,1,config.lstm_num_hidden).to(device)
  c = torch.zeros(config.lstm_num_layers,1,config.lstm_num_hidden).to(device)
  #start_sentence = "sleeping beauty is "
  # uncomment if sentences is not empty
  #len_sen = len(list(start_sentence))
  #sentence = convert_sentence(start_sentence, dataset).to(device)
  for t in range(1, config.seq_length):
    # predict new character
    if feed_start_sentence and t==1:
      _, (h,c) = model(sentence[:-1,:,:],(h,c))
      new_char, (h,c) = model(sentence[-1,:,:].unsqueeze(1),(h,c))
    else:
      new_char, (h,c) = model(new_char,(h,c))

    if use_temp:
      new_char = sample(new_char.squeeze(0).transpose(1,0),temperature)
      result.append(new_char.item())
    else:
      # get the predicted character number
      char_num = torch.max(new_char,dim=1)[1]
      # convert to one hot vector
      new_char = one_hot(char_num.to("cpu"),vocab_size).transpose(1,0)
      # save the resulting character one by one
      result.append(torch.max(new_char.squeeze(0),1)[1].item())
    new_char = new_char.to(device)
  # uncomment if sentences is not empty
  #sentence = start_sentence+dataset.convert_to_string(result)
  sentence = dataset.convert_to_string(result)
  resulting_sentences.append(sentence)
  #print(sentence)
  return resulting_sentences

def test(model, train_vocab_size, config, device, criterion, train_data, test_data):
  model.eval()
  with torch.no_grad():
    print("Starting the Validation Procedure")
    total_loss = 0; total_accuracy = 0; iterations = 0;
    test_data_loader = DataLoader(test_data, config.batch_size, num_workers=0)
    for step, (batch_inputs, batch_targets) in enumerate(test_data_loader):
        batch_inputs = torch.stack(batch_inputs,dim=1).to(device)
        #batch_inputs = one_hot(batch_inputs,train_vocab_size).to(device)
        if batch_inputs.shape[0] != config.batch_size:
          continue
        batch_targets = torch.stack(batch_targets,dim=1).to(device)
        predictions,_ = model(batch_inputs,None)
        loss = criterion(predictions, batch_targets)
        total_accuracy += accuracy(predictions, batch_targets, config.seq_length)
        total_loss += loss.item()
        iterations += 1
    print("Accuracy = {:.3f}, Loss = {:.3f}".format(
            total_accuracy / (iterations+1) , total_loss / (iterations+1)
    ))


def train(config):
    # Initialize the device which to run the model on
    device = ("cuda" if torch.cuda.is_available else "cpu")
    print("Using Device: ", device)
    # Initialize the dataset and data loader (note the +1)
    dataset = TextDataset(config.train_txt_file,config.seq_length)  # fixme
    test_data = TextDataset(config.test_txt_file,config.seq_length)
    char_to_ix_train = dataset._char_to_ix
    ix_to_char_train = dataset._ix_to_char
    test_data.induce_char_ix(char_to_ix_train, ix_to_char_train)
    data_loader = DataLoader(dataset, config.batch_size, num_workers=0)
    vocab_size = dataset._vocab_size
    weights_matrix = np.load("weight_matrix_300.npy")
    print(weights_matrix.shape)
    # Initialize the model that we are going to use
    model = TextGenerationModel(weights_matrix = weights_matrix,
                                seq_length = config.seq_length,
                                vocabulary_size = vocab_size,
                                lstm_num_hidden = config.lstm_num_hidden,
                                lstm_num_layers = config.lstm_num_layers,
                                device = device)  # fixme
    model.to(device)
    # Setup the loss and optimizer
    criterion = CrossEntropyLoss()
    optimizer = optim.RMSprop(model.parameters(), lr=config.learning_rate)  # fixme
    #optimizer = optim.Adam(model.parameters(), lr=config.learning_rate)
    scheduler = ExponentialLR(optimizer, gamma=config.learning_rate_decay)

    total_loss = 0; total_accuracy = 0; iterations = 0; epochs = 0
    model.train()
    while iterations < config.train_steps:
      epochs += 1
      for step, (batch_inputs, batch_targets) in enumerate(data_loader):
          # batch_n X seq length
          batch_inputs = torch.stack(batch_inputs,dim=1).to(device)


          #batch_inputs = one_hot(batch_inputs,vocab_size).to(device)
          #print(batch_inputs.shape)
          #raise ValueError("testing")

          if batch_inputs.shape[0] != config.batch_size:
            continue
          batch_targets = torch.stack(batch_targets,dim=1).to(device)
          # Only for time measurement of step through network
          t1 = time.time()

          predictions,_ = model(batch_inputs,None)

          loss = criterion(predictions, batch_targets)  # fixme
          total_accuracy += accuracy(predictions, batch_targets, config.seq_length)  # fixme
          total_loss += loss.item()
          loss.backward()
          torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=config.max_norm)
          optimizer.step()
          optimizer.zero_grad()
          # Just for time measurement
          t2 = time.time()
          examples_per_second = config.batch_size/float(t2-t1)
          iterations += 1
          if iterations % config.learning_rate_step == 0: scheduler.step()
          if iterations % config.print_every == 0:
              print("[{}] Train Step {:.6g}/{:.6g}, Batch Size = {}, Examples/Sec = {:.2f}, "
                    "Accuracy = {:.2f}, Loss = {:.3f}".format(
                      datetime.now().strftime("%Y-%m-%d %H:%M"), iterations,
                      config.train_steps, config.batch_size, examples_per_second,
                      total_accuracy / (iterations+1) , total_loss / (iterations+1)
              ))
              #test(model, vocab_size, config, device, criterion, dataset, test_data)
              #model.train()
          if iterations % config.sample_every == 0:
              # Generate some sentences by sampling from the model
              print(generate_text(model, config, dataset)[0])
              model.train()
          if iterations % 500 == 0:
              torch.save(model.state_dict(), "lstm_generator_{}_300.pt".format(iterations))

          if step == config.train_steps:
              # If you receive a PyTorch data-loader error, check this bug report:
              # https://github.com/pytorch/pytorch/pull/9655
              break

    print('Done training.')
    torch.save(model.state_dict(), "lstm_generator_300.pt")
    return total_accuracy / (iterations+1)


 ################################################################################
 ################################################################################
def plot_accuracy(LSTM_accuracy):
    print(LSTM_accuracy)
    plt.plot(np.linspace(1,100000,1000),LSTM_accuracy, label="LSTM Accuracy on The Combined Books")
    plt.grid()
    plt.ylabel("Accuracy")
    plt.xlabel("Iterations")
    plt.legend()
    plt.show()

if __name__ == "__main__":

    # Parse training configuration
    parser = argparse.ArgumentParser()

    # Model params
    parser.add_argument('--train_txt_file', type=str, required=True, help="Path to a .txt file to train on")
    parser.add_argument('--test_txt_file', type=str, required=True, help="Path to a .txt file to validate or test on")
    parser.add_argument('--seq_length', type=int, default=30, help='Length of an input sequence')
    parser.add_argument('--lstm_num_hidden', type=int, default=128, help='Number of hidden units in the LSTM')
    parser.add_argument('--lstm_num_layers', type=int, default=2, help='Number of LSTM layers in the model')

    # Training params
    parser.add_argument('--batch_size', type=int, default=128, help='Number of examples to process in a batch')
    parser.add_argument('--learning_rate', type=float, default=4e-3, help='Learning rate')

    # It is not necessary to implement the following three params, but it may help training.
    parser.add_argument('--learning_rate_decay', type=float, default=0.96, help='Learning rate decay fraction')
    parser.add_argument('--learning_rate_step', type=int, default=2500, help='Learning rate step')
    parser.add_argument('--dropout_keep_prob', type=float, default=1.0, help='Dropout keep probability')

    parser.add_argument('--train_steps', type=int, default=2.5e4, help='Number of training steps')
    parser.add_argument('--max_norm', type=float, default=10.0, help='--')

    # Misc params
    parser.add_argument('--summary_path', type=str, default="./summaries/", help='Output path for summaries')
    parser.add_argument('--print_every', type=int, default=200, help='How often to print training progress')
    parser.add_argument('--sample_every', type=int, default=600, help='How often to sample from the model')

    config = parser.parse_args()

    # Train the model
    lstm_res = train(config)

    print("Average accuracy achieved: ", lstm_res)

