from __future__ import print_function
import os

import time
from datetime import datetime
import argparse

import torch
import torch.nn as nn
import numpy as np
from torch.nn import BCEWithLogitsLoss
from torch.distributions import Bernoulli
import matplotlib.pyplot as plt
plt.style.use('ggplot')
#from torchvision.utils import make_grid

import torch
from torch import softmax
from torch import multinomial
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss
from torch.optim.lr_scheduler import LambdaLR
from torch.optim.lr_scheduler import ExponentialLR
from dataset import TextDataset
from VAE_RNN_MODEL import VAE

'''
class Perplexity(NLLLoss):
    """ Language model perplexity loss.
    Perplexity is the token averaged likelihood.  When the averaging options are the
    same, it is the exponential of negative log-likelihood.
    Args:
        weight (torch.Tensor, optional): refer to http://pytorch.org/docs/master/nn.html#nllloss
        mask (int, optional): index of masked token, i.e. weight[mask] = 0.
    """

    _NAME = "Perplexity"
    _MAX_EXP = 100

    def _init_(self, weight=None, mask=None):
        super(Perplexity, self)._init_(weight=weight, mask=mask, size_average=False)

    def eval_batch(self, outputs, target):
        self.acc_loss += self.criterion(outputs, target)
        if self.mask is None:
            self.norm_term += np.prod(target.size())
        else:
            self.norm_term += target.data.ne(self.mask).sum()

    def get_loss(self):
        nll = super(Perplexity, self).get_loss()
        nll /= self.norm_term.item()
        if nll > Perplexity._MAX_EXP:
            print("WARNING: Loss exceeded maximum value, capping to e^100")
            return math.exp(Perplexity._MAX_EXP)
        return math.exp(nll)
'''

def accuracy(preds, targets, seq_length):
  accuracy = 0.0
  with torch.no_grad():
    comparison = torch.max(preds,1)[1] == targets.long()
    accuracy = torch.sum(comparison).item() / (comparison.shape[0]*seq_length)
  return accuracy

def save_elbo_plot(train_curve, val_curve, filename):
    plt.figure(figsize=(12, 6))
    plt.plot(train_curve, label='train elbo')
    plt.plot(val_curve, label='validation elbo')
    plt.legend()
    plt.xlabel('epochs')
    plt.ylabel('ELBO')
    plt.tight_layout()
    plt.savefig(filename)

def save_samples(model, n_samples, path):
  with torch.no_grad():
    samples, means = model.sample(n_samples)
    sampled_images = make_grid(samples, nrow=int(np.sqrt(n_samples)))
    sampled_images_means = make_grid(means, nrow=int(np.sqrt(n_samples)))
    plt.imsave(path, sampled_images.cpu().numpy().transpose(1,2,0))
    plt.imsave("means_"+path, sampled_images_means.cpu().numpy().transpose(1,2,0))

def save_manifold(model, path, n_samples):
  with torch.no_grad():
    linspace = torch.linspace(0,1,n_samples)
    Z1 = (2*torch.erfinv(linspace) - 1) * np.sqrt(2)
    Z2 = (2*torch.erfinv(linspace) - 1) * np.sqrt(2)
    batch = torch.zeros((n_samples**2,2))
    row = 0
    for z1 in Z1:
      for z2 in Z2:
        batch[row,:] = torch.tensor([z1,z2])
        row += 1

    samples = model.decoder(batch.to("cuda")).view(-1,1,28,28)
    sampled_manifold = make_grid(samples, nrow=n_samples)
    plt.imsave(path, sampled_manifold.cpu().numpy().transpose(1,2,0))

def sample_to_string(sample):
  batch_n, sequence_n = sample.shape
  sentences = []
  for batch_i in range(batch_n):
    sentence = []
    for word_i in range(sequence_n):
      word = sample[batch_i, word_i].item()
      sentence.append(ix_to_char_train[word])
    sentences.append(sentence)
  return sentences

def epoch_iter(model, dataloader, optimizer, device):
    """
    Perform a single epoch for either the training or validation.
    use model.training to determine if in 'training mode' or not.

    Returns the average elbo for the complete epoch.
    """
    model.to(device)
    negative_elbo = []
    for step, (batch_inputs, _) in enumerate(dataloader):
      batch_inputs = torch.stack(batch_inputs,dim=1).to(device)
      loss = model(batch_inputs)
      negative_elbo.append(loss.item())
      if step % 20 == 0:
        print(np.mean(negative_elbo))
        model.eval()
        SOS = char_to_ix_train["<SOS>"]
        sample = model.sample(SOS, ARGS.seq_length, 1)
        model.train()
        print(sample_to_string(sample))
      if model.training:
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

    average_epoch_elbo = np.mean(negative_elbo)
    return average_epoch_elbo


def run_epoch(model, train_data, test_data, optimizer, device):
    """
    Run a train and validation epoch and return average elbo for each.
    """

    model.train()
    train_data_loader = DataLoader(train_data, ARGS.batch_size, num_workers=0)
    train_elbo = epoch_iter(model, train_data_loader, optimizer, device)

    #model.eval()
    #val_elbo = epoch_iter(model, test_data, optimizer, device)
    #test_data_loader = DataLoader(test_data, ARGS.batch_size, num_workers=0)

    val_elbo = 0
    return train_elbo, val_elbo

def main():
    device = ("cuda" if torch.cuda.is_available else "cpu")
    train_data = TextDataset(ARGS.train_txt_file,ARGS.seq_length)  # fixme
    test_data = TextDataset(ARGS.test_txt_file,ARGS.seq_length)
    global ix_to_char_train, char_to_ix_train
    char_to_ix_train = train_data._char_to_ix
    ix_to_char_train = train_data._ix_to_char
    test_data.induce_char_ix(char_to_ix_train, ix_to_char_train)
    vocab_size = train_data._vocab_size
    weights_matrix = np.load("weight_matrix_300.npy")
    # Initialize the model that we are going to use
    model = VAE(weights_matrix = weights_matrix,
                seq_length = ARGS.seq_length,
                vocabulary_size = vocab_size,
                lstm_num_hidden = ARGS.lstm_num_hidden,
                lstm_num_layers = ARGS.lstm_num_layers,
                z_dim = ARGS.zdim, device = device)

    model.to("cuda")
    optimizer = torch.optim.Adam(model.parameters())
    train_curve, val_curve = [], []
    for epoch in range(ARGS.epochs):
        elbos = run_epoch(model, train_data, test_data, optimizer, device)
        train_elbo, val_elbo = elbos
        train_curve.append(train_elbo)
        val_curve.append(val_elbo)
        print(f"[Epoch {epoch}] train elbo: {train_elbo} val_elbo: {val_elbo}")

        #if int(ARGS.epochs / 2) == epoch or epoch == 0:
        #  save_samples(model, 64, "Samples_epoch_"+str(epoch)+".png")
    # --------------------------------------------------------------------

    #if ARGS.zdim == 2: save_manifold(model, "manifold.png", 25)
    #save_samples(model, 64, "Samples_epoch_"+str(ARGS.epochs)+".png")
    save_elbo_plot(train_curve, val_curve, 'elbo.png')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--zdim', default=128, type=int,
                        help='dimensionality of latent space')
    parser.add_argument('--train_txt_file', type=str, required=True, help="Path to a .txt file to train on")
    parser.add_argument('--test_txt_file', type=str, required=True, help="Path to a .txt file to validate or test on")
    parser.add_argument('--seq_length', type=int, default=30, help='Length of an input sequence')
    parser.add_argument('--lstm_num_hidden', type=int, default=256, help='Number of hidden units in the LSTM')
    parser.add_argument('--lstm_num_layers', type=int, default=1, help='Number of LSTM layers in the model')

    # Training params
    parser.add_argument('--batch_size', type=int, default=128, help='Number of examples to process in a batch')
    parser.add_argument('--learning_rate', type=float, default=1e-3, help='Learning rate')
    parser.add_argument('--train_steps', type=int, default=2.5e4, help='Number of training steps')
    parser.add_argument('--epochs', type=int, default=20, help='Number of iterations through the dataset')


    ARGS = parser.parse_args()

    main()
