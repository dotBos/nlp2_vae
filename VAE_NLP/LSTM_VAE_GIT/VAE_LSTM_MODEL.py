from __future__ import print_function

import torch
import torch.nn as nn
import numpy as np
from torch.nn import BCEWithLogitsLoss
from torch.distributions import Bernoulli
import matplotlib.pyplot as plt
plt.style.use('ggplot')
#from torchvision.utils import make_grid

import torch
from torch import softmax
from torch import multinomial
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss

class Encoder(nn.Module):

    def __init__(self, weights_matrix, seq_length, vocabulary_size,
                 lstm_num_hidden=128, lstm_num_layers=2, z_dim=20):
        super().__init__()
        self.z_dim = z_dim
        # Embedding
        self.embedding, self.num_embeddings, self.embedding_dim = self.create_emb_layer(torch.tensor(weights_matrix), True)   # Encoder LSTM
        self.embedding_output = None
        self.lstm = nn.LSTM(input_size = self.embedding_dim,
                            hidden_size = lstm_num_hidden,
                            num_layers = lstm_num_layers)

        # Output to latent space
        self.linear_hidden = nn.Sequential(nn.Linear(lstm_num_hidden,z_dim*2),nn.Tanh())
        self.linear_cell = nn.Sequential(nn.Linear(lstm_num_hidden,z_dim*2), nn.Tanh())


    def create_emb_layer(self, weights_matrix, non_trainable=False):
        '''Function to create an embedding given weights'''
        num_embeddings, embedding_dim = weights_matrix.shape
        emb_layer = nn.Embedding(num_embeddings, embedding_dim)
        emb_layer.load_state_dict({'weight': weights_matrix})
        if non_trainable:
            emb_layer.weight.requires_grad = False

        return emb_layer, num_embeddings, embedding_dim

    def forward(self, x, hidden_state=None):
        """
        Perform forward pass of encoder.

        Returns mean and std with shape [batch_size, z_dim]. Make sure
        that any constraints are enforced.
        """

        self.embedding_output = self.embedding(x.long())
        x = self.embedding_output
        #print("input shape: ", x.shape)
        _, (h, c) = self.lstm(x.transpose(1,0), hidden_state)
        #print("output lstm shape encoding: ", output.shape)
        output_hidden = self.linear_hidden(h)
        output_cell = self.linear_hidden(c)
        #print("linear output shape to latent space: ", output.shape)
        mean_hidden, std_hidden = output_hidden[:,:,:self.z_dim], output_hidden[:,:,self.z_dim:]
        mean_cell, std_cell = output_cell[:,:,:self.z_dim], output_cell[:,:,self.z_dim:]

        #print("mean shape: ", mean.shape)
        #print("std shape: ", std.shape)
        return (mean_hidden, std_hidden), (mean_cell,std_cell), (h,c)


class Decoder(nn.Module):

    def __init__(self, weights_matrix, seq_length, vocabulary_size, embedding_dim,
                 lstm_num_hidden=128, lstm_num_layers=2, z_dim=20):
        super().__init__()
        self.z_dim = z_dim
        self.embedding_dim = embedding_dim
        # Decoder
        self.lstm = nn.LSTM(input_size = embedding_dim,
                            hidden_size = lstm_num_hidden,
                            num_layers = lstm_num_layers)

        self.Sequential = nn.Sequential(nn.Linear(lstm_num_hidden, vocabulary_size))

    def forward(self, hidden_state, cell_state, embedding_output):
        """
        Perform forward pass of encoder.

        Returns mean with shape [batch_size, 784].
        """
        output, (h, c) = self.lstm(embedding_output, (hidden_state,cell_state))
        mean = self.Sequential(output)
        return mean, (h,c)


class VAE(nn.Module):

    def __init__(self, weights_matrix, seq_length, vocabulary_size,
                 lstm_num_hidden=128, lstm_num_layers=2, z_dim=20, device='cuda:0'):
        super().__init__()
        self.device = device
        self.z_dim = z_dim
        self.embedding = None
        self.vocabulary_size = vocabulary_size
        self.encoder = Encoder(weights_matrix, seq_length, vocabulary_size,
                               lstm_num_hidden, lstm_num_layers, z_dim)
        self.embedding_dim = self.encoder.embedding_dim
        self.linear_hidden = nn.Linear(z_dim, lstm_num_hidden)
        self.linear_cell = nn.Linear(z_dim, lstm_num_hidden)
        self.decoder = Decoder(weights_matrix, seq_length, vocabulary_size, self.embedding_dim,
                               lstm_num_hidden, lstm_num_layers, z_dim)


    def one_hot(self, batch, vocab_size):
      '''Input: batch_n x input_seq '''
      batch_n, seq_num = batch.shape
      one_hot = torch.zeros(seq_num, batch_n, vocab_size)
      for i in range(batch_n):
        for j in range(seq_num):
          one_hot[j,i,batch[i,j].item()] = 1
      return one_hot.transpose(1,0).transpose(2,1)

    def forward(self, input):
        """
        Given input, perform an encoding and decoding step and return the
        negative average elbo for the given batch.
        """
        # hidden mean/std, cell mean/std
        (mean_hidden, std_hidden), (mean_cell, std_cell),_ = self.encoder.forward(input)
        self.embedding = self.encoder.embedding
        epsilon = torch.rand(std_hidden.shape)

        z_hidden = self.reparameterization(mean_hidden, std_hidden, epsilon)
        z_cell = self.reparameterization(mean_cell, std_cell, epsilon)
        hidden_state = self.linear_hidden(z_hidden)
        cell_state = self.linear_cell(z_cell)
        output, _ = self.decoder.forward(hidden_state, cell_state, self.encoder.embedding_output.transpose(1,0))
        output = output.transpose(2,1).transpose(0,2)
        # The Binary Cross entropy loss, the reconstruction loss
        criterion = CrossEntropyLoss()
        BCE = criterion(output, input)
        epsilon = 1e-6

        KL_divergence_hidden = torch.mean(-0.5*torch.sum(2*torch.log(std_hidden.pow(2) + epsilon) - std_hidden.pow(2) - mean_hidden.pow(2) + 1, dim=2),dim=1).mean()
        KL_divergence_cell = torch.mean(-0.5*torch.sum(2*torch.log(std_cell.pow(2) + epsilon) - std_cell.pow(2) - mean_cell.pow(2) + 1, dim=2),dim=1).mean()

        average_negative_elbo = BCE + KL_divergence_hidden + KL_divergence_cell
        return average_negative_elbo

    def reparameterization(self, mean, std, epsilon):
      z = mean + std * epsilon.to("cuda")
      return z

    def sample(self, SOS, seq_length, n_samples):
        """
        Sample n_samples from the model. Return both the sampled images
        (from bernoulli) and the means for these bernoullis (as these are
        used to plot the data manifold).
        """
        with torch.no_grad():
          z = torch.randn(1, n_samples, self.z_dim).to("cuda")
          #sampled_z_cell = torch.randn(1, n_samples, self.z_dim).to("cuda")
          hidden_state = self.linear_hidden(z)
          cell_state = self.linear_cell(z)
          input = torch.tensor([[SOS]]).to("cuda")
          embedding_output = self.embedding(input)
          generated, (h,c) = self.decoder(hidden_state, cell_state, embedding_output)
          generated = (generated).float()
          generated = torch.argmax(generated, dim=2)
          sampled_sentence = []
          for i in range(seq_length):
            embedding_output = self.embedding(generated)
            generated, (h,c) = self.decoder(h, c, embedding_output)
            generated = (generated).float()
            generated = torch.argmax(generated, dim=2)
            #print("sampled shape: ", generated.shape)
            sampled_sentence.append(generated.squeeze(0))
            #raise ValueError("hi")
          sampled_sentence = torch.stack(sampled_sentence)
          #print(sampled_sentence.shape)
          return sampled_sentence.transpose(1,0)
