import numpy as np
import pickle
target_vocab = np.load("vocabulary.npy")
glove = pickle.load( open( "glove_300.p", "rb" ) )
matrix_len = len(target_vocab)
weights_matrix = np.zeros((matrix_len, 300))
words_found = 0
emb_dim = 300
for i, word in enumerate(target_vocab):
    try:
        weights_matrix[i] = glove[word]
        words_found += 1
    except KeyError:
      weights_matrix[i] = np.random.normal(scale=0.8, size=(emb_dim, ))
np.save("weight_matrix_300", weights_matrix)
