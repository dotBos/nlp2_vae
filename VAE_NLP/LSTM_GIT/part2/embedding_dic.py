import numpy as np
import bcolz
import pickle

glove_path = "Glove/glove.6B"
vectors = bcolz.open(f'{glove_path}/6B.300.dat')[:]
words = pickle.load(open(f'{glove_path}/6B.300_words.pkl', 'rb'))
word2idx = pickle.load(open(f'{glove_path}/6B.300_idx.pkl', 'rb'))
glove = {w: vectors[word2idx[w]] for w in words if not word2idx[w] >= 400000}
pickle.dump( glove, open( "glove_300.p", "wb" ) )
print("Embedding for The: ",glove["the"])
