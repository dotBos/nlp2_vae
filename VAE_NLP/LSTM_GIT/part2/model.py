# MIT License
#
# Copyright (c) 2017 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2018
# Date Created: 2018-09-04
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch.nn as nn
import torch


class TextGenerationModel(nn.Module):

    def __init__(self, weights_matrix, seq_length, vocabulary_size,
                 lstm_num_hidden=256, lstm_num_layers=2, device='cuda:0'):

        super(TextGenerationModel, self).__init__()
        # Initialization here...
        #self.batch_size = batch_size
        # expects input of shape (seq_len, batch, input_size)
        #self.embedding = nn.Sequential(nn.Linear(vocabulary_size, vocabulary_size),
        #                               nn.Tanh(),
        #                               nn.Dropout(0.3))
        self.embedding, num_embeddings, embedding_dim = self.create_emb_layer(torch.tensor(weights_matrix), True)
        self.lstm = nn.LSTM(input_size = embedding_dim,
                            hidden_size = lstm_num_hidden,
                            num_layers = lstm_num_layers)
        self.linear = nn.Linear(lstm_num_hidden,vocabulary_size)
        #self.dropout = nn.Dropout(0.3)
        # we could also default these to zero, just not provide as input

    def forward(self, x, hidden_state):
        # Implementation here...
        x = self.embedding(x.long())

        output, (h, c) = self.lstm(x.transpose(1,0), hidden_state)
        output = self.linear(output)
        #output = self.dropout(output)
        # output is a tensor of shape (batch, vocab_length, seq_length)
        return output.transpose(1,0).transpose(2,1), (h,c)

    def create_emb_layer(self, weights_matrix, non_trainable=False):
      '''boiler plate function to create an embedding'''
      num_embeddings, embedding_dim = weights_matrix.shape
      emb_layer = nn.Embedding(num_embeddings, embedding_dim)
      emb_layer.load_state_dict({'weight': weights_matrix})
      if non_trainable:
          emb_layer.weight.requires_grad = False

      return emb_layer, num_embeddings, embedding_dim

